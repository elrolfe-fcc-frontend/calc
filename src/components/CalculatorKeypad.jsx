import React, { Component } from "react";

import "./CalculatorKeypad.css";

class CalculatorKeypad extends Component {
  render() {
    return (
      <div className="calculator-keypad">
        <button onClick={this.props.onEntry} className="button-memory" id="memory-clear">MC</button>
        <button onClick={this.props.onEntry} className="button-memory" id="memory-recall">MR</button>
        <button onClick={this.props.onEntry} className="button-memory" id="memory-add">M+</button>
        <button onClick={this.props.onEntry} className="button-memory" id="memory-subtract">M-</button>
        <button onClick={this.props.onEntry} className="button-clear" id="clear">C</button>

        <button onClick={this.props.onEntry} className="button-number" id="seven">7</button>
        <button onClick={this.props.onEntry} className="button-number" id="eight">8</button>
        <button onClick={this.props.onEntry} className="button-number" id="nine">9</button>
        <button onClick={this.props.onEntry} className="button-unary" id="square">x<sup>2</sup></button>
        <button onClick={this.props.onEntry} className="button-unary" id="square-root">&radic;<span>x</span></button>

        <button onClick={this.props.onEntry} className="button-number" id="four">4</button>
        <button onClick={this.props.onEntry} className="button-number" id="five">5</button>
        <button onClick={this.props.onEntry} className="button-number" id="six">6</button>
        <button onClick={this.props.onEntry} className="button-operator" id="multiply">&times;</button>
        <button onClick={this.props.onEntry} className="button-operator" id="divide">&divide;</button>

        <button onClick={this.props.onEntry} className="button-number" id="one">1</button>
        <button onClick={this.props.onEntry} className="button-number" id="two">2</button>
        <button onClick={this.props.onEntry} className="button-number" id="three">3</button>
        <button onClick={this.props.onEntry} className="button-operator" id="add">+</button>
        <button onClick={this.props.onEntry} className="button-operator" id="subtract">-</button>

        <button onClick={this.props.onEntry} className="button-unary" id="percent">%</button>
        <button onClick={this.props.onEntry} className="button-number" id="zero">0</button>
        <button onClick={this.props.onEntry} className="button-decimal" id="decimal">.</button>
        <button onClick={this.props.onEntry} className="button-unary" id="negate">&plusmn;</button>
        <button onClick={this.props.onEntry} className="button-equals" id="equals">=</button>
      </div>
    );
  }
}

export default CalculatorKeypad;
