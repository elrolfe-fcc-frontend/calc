import React, { Component } from 'react';
import Calculator from "./Calculator";

class App extends Component {
  render() {
    return (
      <div className="App">
        <header>
          <h1>JavaScript Calculator</h1>
        </header>
        <main>
          <Calculator />
        </main>
        <footer>
          <p>Coded by <a href="https://www.freecodecamp.org/elrolfe" target="_blank" rel="noopener noreferrer">Eric Rolfe</a></p>
        </footer>
      </div>
    );
  }
}

export default App;
