import React, { Component } from "react";

import "./CalculatorMode.css";

class CalculatorMode extends Component {
  render() {
    const modeClass = `calculator-mode${this.props.mode === CalculatorMode.MODE_IMMEDIATE ? " immediate" : ""}`;
    return (
      <div className={modeClass}>
        <p>Calculation Mode</p>
        <span>Immediate <button className="button-mode" onClick={this.props.onToggle}>&nbsp;</button> Formula</span>
      </div>
    );
  }
}

CalculatorMode.MODE_IMMEDIATE = 1;
CalculatorMode.MODE_FORMULA = 2;

export default CalculatorMode;
