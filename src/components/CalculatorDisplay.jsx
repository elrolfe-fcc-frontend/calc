import React, { Component } from "react";

import "./CalculatorDisplay.css";

class CalculatorDisplay extends Component {
  render() {
    return (
      <div className="calculator-display">
        <div className="equation">{this.props.equation}</div>
        <p className="entry" id="display">{this.props.entry}</p>
        <p className="memory">{this.props.memory ? "M" : ""}</p>
      </div>
    );
  }
}

export default CalculatorDisplay;
