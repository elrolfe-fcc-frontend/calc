import React, { Component } from "react";
import CalculatorDisplay from "./CalculatorDisplay";
import CalculatorKeypad from "./CalculatorKeypad";

import "./Calculator.css";

const STATE_CLEAR = "STATE_CLEAR";
const STATE_ERROR = "STATE_ERROR";
const STATE_NUMBER = "STATE_NUMBER";
const STATE_DECIMAL = "STATE_DECIMAL";
const STATE_OPERATOR = "STATE_OPERATOR";
const STATE_RESULT = "STATE_RESULT";
const STATE_UNARY = "STATE_UNARY";

class Calculator extends Component {
  constructor(props) {
    super(props);

    this.state = {
      entry: "0",
      equation: "",
      memory: 0,
      processState: STATE_CLEAR,
      operation: "+",
      value: 0
    };

    this.processEntry = this.processEntry.bind(this);
  }

  clear() {
    this.setState({
      entry: "0",
      equation: "",
      processState: STATE_CLEAR,
      operation: "+",
      value: 0
    });
  }

  decimal() {
    let entry;

    switch (this.state.processState) {
      case STATE_OPERATOR:
      case STATE_RESULT:
      case STATE_UNARY:
        entry = "0.";
        break;

      case STATE_CLEAR:
      case STATE_NUMBER:
        entry = `${this.state.entry}.`;
        break;

      case STATE_ERROR:
      case STATE_DECIMAL:
      default: return;
    }

    this.setState({
      entry,
      processState: STATE_DECIMAL
    });
  }

  equals() {
    let entry;
    let result;

    switch (this.state.processState) {
      case STATE_NUMBER:
      case STATE_DECIMAL:
      case STATE_OPERATOR:
      case STATE_RESULT:
      case STATE_UNARY:
        // eslint-disable-next-line
        result = eval(this.state.value + this.state.operation + this.state.entry);
        if (isFinite(result)) {
          entry = String(this.round(result, 8));
        } else {
          this.error();
          return;
        }
        break;

      case STATE_CLEAR:
      case STATE_ERROR:
      default: return;
    }

    this.setState({
      entry,
      equation: "",
      operation: "+",
      value: 0,
      processState: STATE_RESULT
    });
  }

  error() {
    this.setState({
      equation: "",
      entry: "Error",
      processState: STATE_ERROR
    });
  }

  memory(target) {
    let memory = this.state.memory;
    let entry = this.state.entry;
    let processState = this.state.processState;

    switch (processState) {
      case STATE_CLEAR:
      case STATE_NUMBER:
      case STATE_DECIMAL:
      case STATE_OPERATOR:
      case STATE_RESULT:
      case STATE_UNARY:
        switch (target.id) {
          case "memory-clear":
            memory = 0;
            break;

          case "memory-recall":
            entry = String(this.round(memory, 8));
            processState = entry.includes(".") ? STATE_DECIMAL : STATE_NUMBER;
            break;

          case "memory-add":
            memory += this.round(entry);
            break;

          case "memory-subtract":
            memory -= this.round(entry);
            break;

          default: return;
        }
        break;

      case STATE_ERROR:
      default: return;
    }

    this.setState({
      entry,
      memory,
      processState
    });
  }

  number(target) {
    let entry = this.state.entry;
    let processState = this.state.processState;

    switch (processState) {
      case STATE_CLEAR:
      case STATE_NUMBER:
        processState = STATE_NUMBER;
        if (entry === "0") {
          entry = target.innerText;
        } else {
          entry = `${entry}${target.innerText}`;
        }
        break;

      case STATE_DECIMAL:
        entry = `${entry}${target.innerText}`;
        break;

      case STATE_OPERATOR:
      case STATE_RESULT:
      case STATE_UNARY:
        processState = STATE_NUMBER;
        entry = target.innerText;
        break;

      case STATE_ERROR:
      default: return;
    }

    this.setState({
      entry,
      processState
    });
  }

  operator(target) {
    let equation = this.state.equation;
    let entry = this.state.entry;
    let value = this.state.value;
    let operation =
      target.id === "add" ? "+" :
      target.id === "subtract" ? "-" :
      target.id === "multiply" ? "*" : "/";

    switch (this.state.processState) {
      case STATE_CLEAR:
      case STATE_NUMBER:
      case STATE_DECIMAL:
      case STATE_RESULT:
      case STATE_UNARY:
        // eslint-disable-next-line
        value = eval(this.state.value + this.state.operation + entry);
        if (!isFinite(value)) {
          this.error();
          return;
        }

        equation = `${equation}${equation.length ? " " : ""}${entry} ${operation}`;
        entry = String(this.round(value, 8));
        break;

      case STATE_OPERATOR:
        equation = `${equation.substring(0, equation.length - 1)}${operation}`;
        break;

      case STATE_ERROR:
      default: return;
    }

    this.setState({
      entry,
      equation,
      operation,
      value,
      processState: STATE_OPERATOR
    });
  }

  processEntry({ target }) {
    switch (target.className) {
      case "button-clear":
        this.clear();
        break;

      case "button-decimal":
        this.decimal();
        break;

      case "button-equals":
        this.equals();
        break;

      case "button-memory":
        this.memory(target);
        break;

      case "button-number":
        this.number(target);
        break;

      case "button-operator":
        this.operator(target);
        break;

      case "button-unary":
        this.unary(target);
        break;

      default:
        this.error();
        break;
    }
  }

  render() {
    return (
      <div className="calculator">
        <CalculatorDisplay
          entry={this.state.entry}
          equation={this.state.equation}
          memory={this.state.memory}
        />
        <p className="label">FreeCodeCamp.org</p>
        <CalculatorKeypad
          onEntry={this.processEntry}
        />
      </div>
    );
  }

  round(number, precision = 0) {
    const shift = (number, precision, reverseShift) => {
      if (reverseShift) {
        precision = -precision;
      }

      var numArray = ("" + number).split("e");
      return +(
        numArray[0] +
        "e" +
        (numArray[1] ? +numArray[1] + precision : precision)
      );
    };

    return shift(Math.round(shift(number, precision, false)), precision, true);
  }

  unary(target) {
    let entry = this.state.entry;
    let result;

    switch (this.state.processState) {
      case STATE_CLEAR:
      case STATE_NUMBER:
      case STATE_DECIMAL:
      case STATE_OPERATOR:
      case STATE_RESULT:
        switch (target.id) {
          case "square-root":
            result = this.round(Math.sqrt(parseFloat(entry)), 8);
            break;

          case "square":
            result = this.round(parseFloat(entry) ** 2, 8);
            break;

          case "percent":
            result = this.round(parseFloat(entry) / 100, 8);
            break;

          case "negate":
            result = this.round(-1 * parseFloat(entry), 8);
            break;

          default: return;
        }
        break;

      case STATE_ERROR:
      default: return;
    }

    if (!isFinite(result)) {
      this.error();
      return;
    }

    entry = String(result);
    this.setState({
      entry,
      processState: STATE_UNARY
    });
  }
}

export default Calculator;
